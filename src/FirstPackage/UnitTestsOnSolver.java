package FirstPackage;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Iterator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class UnitTestsOnSolver {
	private Solver s, si, sii;
	private final String input1 = "C:\\Users\\pheng\\Documents\\cs4244 kb\\Stage1\\RTI_k3_n100_m429_101.cnf";
	private final String inputAye = "C:\\Users\\pheng\\Documents\\cs4244 kb\\Stage1\\an-unsat-instance.txt";
	private final String inputTwo = "C:\\Users\\pheng\\Documents\\cs4244 kb\\Stage1\\a-sat-instance.txt";
	
	@BeforeEach
	public void setUp() {
		s = new Solver();
		si = new Solver();
		sii = new Solver();
		
		try {
			s.readClauses(input1);
			si.readClauses(inputAye);
			sii.readClauses(inputTwo);
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.getMessage());
			
		}
		
	}
	
	@Test
	void testReadingInput() {
		ArrayList<String> actualClauseAtIndexZero = null;
		try {
			actualClauseAtIndexZero = s.getClauses().get(0);
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.getMessage());

		}
		ArrayList<String> expectedClause = new ArrayList<>();
		expectedClause.add("-58");
		expectedClause.add("-79");
		expectedClause.add("93");
		Iterator<String> it = expectedClause.iterator();
		int index = 0;
		while (it.hasNext()) {
			assertEquals(it.next(), actualClauseAtIndexZero.get(index));
			index++;
		}
	}
	
	@Test
	void testUnsat() {
		String result = null;
		try {
			result = si.solve();
		} catch (Exception e) {
			
			e.printStackTrace();
			fail(e.getMessage());
		}
		String expected = si.UNSAT_MESSAGE;
		assertEquals(expected, result);
	}
	
	@Test
	void testSat() {
		String result = null;
		try {
			result = si.solve();
		} catch (Exception e) {
			fail(e.getMessage());
			e.printStackTrace();
		}
		String expected = "1 -2 3";
		assertEquals(expected, result);
	}

}
