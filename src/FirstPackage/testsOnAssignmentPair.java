package FirstPackage;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class testsOnAssignmentPair {

	@Test
	void defaultValueShallBeUnassigned() {
		String xTheLiteral = "3";
		AssignmentPair p = new AssignmentPair(xTheLiteral);
		assertEquals(AssignmentPair.UNASSIGNED, p.getAssignment());
	}

}
