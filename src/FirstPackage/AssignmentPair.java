package FirstPackage;

import java.util.Objects;

public class AssignmentPair {
	private String literalAsString;
	private String value;
	
	public final static String UNASSIGNED = "u";

	public AssignmentPair(String xTheLiteral) {
		this.literalAsString = xTheLiteral;
		this.value = this.UNASSIGNED;
	}

	public Object getLiteralAsString() {
		return this.literalAsString;
	}

	public boolean hasNoAssignment() {
		if (Objects.equals(this.value, UNASSIGNED)) {
			return true;
		}
		return false;
	}

	public String getAssignment() {
		return this.value;
	}

}
