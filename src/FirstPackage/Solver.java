package FirstPackage;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Objects;

public class Solver {

	
	
	private final int INDEX_OF_MARKER = 0;
	private final String MARKER_FOR_COMMENT = "c";
	private final String MARKER_FOR_METADATA = "p";
	private final int EXPECTED_LENGTH_OF_LINE_ON_METADATA = 4;
	private final String lacksMetadataMessage = "EXPECTED "
	    + EXPECTED_LENGTH_OF_LINE_ON_METADATA
	    + " number of tokens on the line that starts with "
	    + MARKER_FOR_METADATA
	    + " but received fewer tokens than that.";
	private final int INDEX_OF_THE_STRING_CNF = 1;
	private final String THE_STRING_CNF = "cnf";
	private final String cnfNotFoundMessage = "Expected to detect the string "
			+ THE_STRING_CNF
			+ " at the index "
			+ INDEX_OF_THE_STRING_CNF
		    + " of the tokens-array of the line that starts with "
		    + MARKER_FOR_METADATA 
		    + " but did not detect it.";
	
	private final Integer NOT_DEFINED = Integer.MIN_VALUE;
	private final int INDEX_OF_NUMBER_OF_CLAUSES = 3;
	private int INDEX_OF_NUMBER_OF_VARIABLES = 2;
	private final String notDefinedMessage = "Expected that either numberOfClauses or numberOfVariables (or both) "
			+ "to be defined, but at least one of them (or both) aren't defined (as far as I can tell)";
	public final String UNSAT_MESSAGE = "UNSATISFIABLE";
	private final String CONFLICT_CONDITION = "All literals have been assigned the value 0";
	
	private ArrayList<ArrayList<String>> clauses;
	private BufferedReader br;
	private Integer numberOfClauses;
	private Integer numberOfVariables;
	private ArrayList<ArrayList<AssignmentPair>> assignments;
	private String END_OF_LINE = "0";

	
	
	public Solver() {
		numberOfClauses = NOT_DEFINED;
		numberOfVariables = NOT_DEFINED;
		clauses = new ArrayList<ArrayList<String>>();
		assignments = new ArrayList<ArrayList<AssignmentPair>>();
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	public void readClauses(String dir) throws Exception {
		String line = "";
		String regexForASpace = "\\s+"; // TODO what is it
		br = new BufferedReader(new FileReader(dir));

		while ((line = br.readLine()) != null) {
			// TODO write unit tests that enter each if-conditional
			String[] tokens = line.trim().split(regexForASpace);
			if (Objects.equals(tokens[INDEX_OF_MARKER], MARKER_FOR_COMMENT)) {
				continue;
			}
			
			if (Objects.equals(tokens[INDEX_OF_MARKER], MARKER_FOR_METADATA)) {
				if (tokens.length != EXPECTED_LENGTH_OF_LINE_ON_METADATA) {
					throw new Exception(lacksMetadataMessage);
				}
				
				boolean isTheStringCnf = Objects.equals(tokens[INDEX_OF_THE_STRING_CNF], THE_STRING_CNF);
				if (!isTheStringCnf) {
					throw new Exception(cnfNotFoundMessage );
				}
				
				numberOfClauses = Integer.parseInt(tokens[INDEX_OF_NUMBER_OF_CLAUSES ]);
				numberOfVariables = Integer.parseInt(tokens[INDEX_OF_NUMBER_OF_VARIABLES ]);
				continue;
			}
			ArrayList<String> clause = new ArrayList<>();
			for (int i=0; i<tokens.length; i++) {
				if (!Objects.equals(tokens[i], END_OF_LINE)) {
					clause.add(tokens[i]);
				}
			}
			clauses.add(clause);
			
		}
		
		if (numberOfClauses == NOT_DEFINED || numberOfVariables == NOT_DEFINED) {
			throw new Exception(notDefinedMessage );
		}
		
		br.close();
	}

	public ArrayList<ArrayList<String>> getClauses() throws Exception {
		if (clauses == null) {
			throw new Exception("I don't think the data-structure 'clauses' has been initialised/populated properly.");
		}
		return clauses;
	}
	public String solve() throws Exception {
		
		/*
		 * CDCL(ϕ, ν) 
		 * 1    if (UnitPropagation(ϕ,ν) == CONFLICT)
		 * 2    then return UNSAT 
		 * 3    dl ← 0 // Decision level
		 * 4    while (not AllVariablesAssigned(ϕ, ν))
		 * 5        do (x, v) = PickBranchingVariable(ϕ, ν) // Decide stage. note that 'ν' is a different character from 'v'  
		 * 6            dl ← dl + 1 // Increment decision level due to new decision
		 * 7            ν ← ν ∪ {(x,v)} 
		 * 8            if (UnitPropagation(ϕ, ν) == CONFLICT) // Deduce stage 
		 * 9                then β = ConflictAnalysis(ϕ, ν) // Diagnose stage 
		 * 10                    if (β < 0) 
		 * 11                       then return UNSAT 
		 * 12                       else Backtrack(ϕ,ν, β)
		 * 13                            dl ← β // Decrement decision level due to backtracking
		 * 14   return SAT
		 *     
		 *     The above algorithm has been reproduced from the text:
		 *     Marques-Silva, et al. (2008). 'Chapter 4: Conflict-driven clause learning sat solvers'. Handbook
		 *     of Satisfiability. IOS Press.
		 */
		String result = null;
		String propagated = this.propagateUnit(this.getClauses(), this.getAssignments());
		if (Objects.equals(propagated, CONFLICT_CONDITION)) {
			result = UNSAT_MESSAGE;
		}
		
		return result;
	}
	
	private String propagateUnit(ArrayList<ArrayList<String>> clauses2, ArrayList<ArrayList<AssignmentPair>> assignments) {
		/*
		 * used for identifying variables which must be assigned a speciﬁc Boolean value.
		 * 
		 * the unit clause rule: 
		 * if a clause is unit, then its sole unassigned literal must be assigned value 1 
		 * for the clause to be satisﬁed.
		 *
		 * If an unsatisﬁed clause is identiﬁed, a conﬂict condition is declared, and the 
		 * algorithm backtracks.
		 * 
		 * (Marques-Silva, et al, 2008) 
		 */
		Iterator<ArrayList<String>> it = clauses2.iterator();
		while (it.hasNext()) {
			ArrayList<ArrayList<AssignmentPair>> unionWithSoleUnassignedLiteral;
			ArrayList<String> currentClause = it.next();
			if (Clause.isUnitClause(currentClause, assignments)) {
				unionWithSoleUnassignedLiteral = setAssignment(Clause.getSoleUnassignedLiteral(currentClause), assignments);
			}
			if (Clause.hasUnsatisfiedClause(clauses2, unionWithSoleUnassignedLiteral)) {
				return this.CONFLICT_CONDITION;
			}
		}
		
	}
	private ArrayList<ArrayList<AssignmentPair>> getAssignments() {
		return this.assignments;
	}

}
