package FirstPackage;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Objects;

public class Clause {

	public static boolean isUnitClause(ArrayList<String> currentClause,
			ArrayList<ArrayList<AssignmentPair>> assignments) {
		/* the unit clause rule: 
		 * if a clause is unit, then its sole unassigned literal must be assigned value 1 
		 * for the clause to be satisﬁed.
		 */
		Iterator<String> it = currentClause.iterator();
		int numberOfUnassignedLiterals = 0;
		while (it.hasNext()) {
			String literal = it.next();
			if (Clause.hasNoAssignment(literal, assignments)) {
				numberOfUnassignedLiterals++;
			}
		}
		if (numberOfUnassignedLiterals == 1) {
			return true;
		}
		return false;
	}

	private static boolean hasNoAssignment(String literal, ArrayList<ArrayList<AssignmentPair>> assignments) {
		Iterator<ArrayList<AssignmentPair>> iteratorOverClauses = assignments.iterator();
		Iterator<AssignmentPair> iteratorOverLiterals;
		ArrayList<AssignmentPair> currentClause;
		AssignmentPair currentLiteral;
		while (iteratorOverClauses.hasNext()) {
			currentClause = iteratorOverClauses.next();
			iteratorOverLiterals = currentClause.iterator();
			while (iteratorOverLiterals.hasNext()) {
				currentLiteral = iteratorOverLiterals.next();
				if (Objects.equals(literal, currentLiteral.getLiteralAsString())
						&& currentLiteral.hasNoAssignment()) {
					return true;
				}
			}
		}
		return false;
	}

}
